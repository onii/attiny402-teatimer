# attiny402-teatimer

A little 6 minute tea timer. Based on 
[my other project](https://gitlab.com/onii/attiny402-petfedbeacon/).

![Schematic](IMG_20200225_184548.jpg)
