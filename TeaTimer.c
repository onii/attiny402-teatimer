/* Tea timer v2
 *  A "simple" timer with buzzer for attiny402
 */

// --- Clock speed ---
#ifndef F_CPU
#define F_CPU 32000UL // 32k
#endif

// --- Pin layout ---
#define pushPin PORTA.PIN6CTRL
#define pushPinInt PORT_INT6_bm
#define pushPinBM PIN6_bm
#define ledPin PIN1_bm
#define buzzPin PIN7_bm

#define unusedPin1 PORTA.PIN2CTRL
#define unusedPin2 PORTA.PIN3CTRL

// --- Constants ---
#define totalTime 90 // 4*totalTime(seconds) = timer dur... (ex. 90 * 4s = 6min)
#define buzzAmt 2    // How many buzzes will each cycle of the final alarm have
#define buzzCycle 2  // How many times will buzzAmt run
#define VCCthresh 3540 // >3.54v == "good" threshold
#define shortDelay 25  // Milliseconds

// --- Includes ---
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
// ^ Requires "-Wa,-mgcc-isr" avr-gcc flags

// --- Global vars ---
volatile uint8_t btnPushed = 0;

// --- Interrupts ---
ISR(PORTA_PORT_vect) {               // Pin interrupt ISR
  PORTA.INTFLAGS = pushPinInt;       // Clear flag by writing "1" to position
  pushPin |= PORT_ISC_INTDISABLE_gc; // Disable pin ISR
  btnPushed = 1;
}

ISR(RTC_PIT_vect) {            // RTC interrupt ISR
  RTC.PITINTFLAGS = RTC_PI_bm; // Clear flag by writing "1" to position
}

// -- Functions ---
uint8_t measureVCC() {
  // Copied/modified from:
  // https://hackaday.io/project/165439

  VREF.CTRLA = VREF_ADC0REFSEL_1V1_gc; /* Set the Vref to 1.1V*/
  ADC0.MUXPOS = ADC_MUXPOS_INTREF_gc;  /* ADC internal reference, the Vbg*/

  ADC0.CTRLC = ADC_PRESC_DIV4_gc      /* CLK_PER divided by 4 */
               | ADC_REFSEL_VDDREF_gc /* Vdd (Vcc) be ADC reference */
               | 0 << ADC_SAMPCAP_bp;
  /* Sample Capacitance Selection: disabled */

  ADC0.CTRLA = 1 << ADC_ENABLE_bp     /* ADC Enable: enabled */
               | ADC_RESSEL_10BIT_gc; /* 10-bit mode */

  _delay_ms(2); // Necessary to stabilize reading

  ADC0.COMMAND |= 1; // start running ADC

  while (1) {
    if (ADC0.INTFLAGS) // if an ADC result is ready
    {
      uint16_t Vcc_value = 0;                 /* measured Vcc value */
      Vcc_value = (0x400 * 1100L) / ADC0.RES; /* calculate the Vcc value */
      ADC0.CTRLA = 0 << ADC_ENABLE_bp;        // Disable ADC
      if (Vcc_value > VCCthresh) {
        return (1);
      } else {
        return (0);
      }
    }
  }
}

uint8_t beepBuzzer(uint8_t buzzCount) {
  // Sound the buzzer
  for (uint8_t buzzLoop = 0; buzzLoop < buzzCount; buzzLoop++) {
    PORTA.OUTSET = buzzPin; // Buzzer HIGH
    _delay_ms((shortDelay * 2));
    PORTA.OUTCLR = buzzPin; // Buzzer LOW
    _delay_ms((shortDelay * 10));
  }
  return 0;
}

uint8_t softReset(void) {
  _PROTECTED_WRITE(RSTCTRL.SWRR, RSTCTRL_SWRE_bm); // Enable soft reset
  RSTCTRL.RSTFR = RSTCTRL_SWRF_bm; // Soft reset command
}

uint8_t goToSleep(void) {
  SLPCTRL.CTRLA = SLPCTRL_SEN_bm | SLPCTRL_SMODE_PDOWN_gc;
  // Enable and set pwr down mode
  __asm__ __volatile__ ( "sleep" "\n\t" :: ); // sleep_cpu() equiv
  // ^ Copied from: https://hackaday.io/project/165881
  SLPCTRL.CTRLA &= ~SLPCTRL_SEN_bm; // Disable sleep
  return 0;
}

uint8_t writeEEPROM(void) {
  uint16_t eepRead = eeprom_read_word((uint16_t*)0);
  if (eepRead == 65535U) { // Nothing in EEPROM location 0
    eeprom_write_word((uint16_t*)0, 0); // Write 0 val to location 0
    eepRead = 0;
  }
  eepRead++;
  eeprom_write_word((uint16_t*)0, eepRead); // Write val+1 to location 0
  //eeprom_write_word((uint16_t*)0, 65535U); // If you wanna clear EEPROM
}

uint8_t pulseLED (void) {
  // Pulse LED to save power
  
  for (uint8_t ledCounter = 0; ledCounter < 3; ledCounter++) {
    PORTA.OUTSET = ledPin;
    _delay_ms(1);
    PORTA.OUTCLR = ledPin;
    _delay_ms(6);
  }
  
  return 0;
}

uint8_t buttonPress(void) {
  // --- Turn on led and wait until button released ---

  uint8_t goodVCC = measureVCC(); // Measure VCC here

  if (goodVCC == 0) {
    beepBuzzer((buzzAmt * 2)); // Warn about the battery level
  }

  do {
    pulseLED();
    pulseLED();
  } while ((PORTA.IN & pushPinBM) == 0);
  
  _delay_ms(50);
  // Un-noticed delay after LED off, to hopefully prevent false-positives

  return 0;
}

// --- Main ---

int main(void) {
  // --- Pin modes ---
  PORTA.DIRSET = ledPin | buzzPin; // Set LED & buzzer as output
  pushPin = PORT_PULLUPEN_bm;
  // Enable pullup

  unusedPin1 = PORT_PULLUPEN_bm | PORT_ISC_INPUT_DISABLE_gc;
  unusedPin2 = PORT_PULLUPEN_bm | PORT_ISC_INPUT_DISABLE_gc;
  // Pullup unused pins & disable digital input buffer

  // --- Clock speed ---
  /* Set the Main clock to internal 32kHz oscillator*/
  _PROTECTED_WRITE(CLKCTRL.MCLKCTRLA, CLKCTRL_CLKSEL_OSCULP32K_gc);
  /* Set Main clock prescaler div to 2X and disable the Main clock prescaler */
  _PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, CLKCTRL_PDIV_2X_gc); // without enable
  /* ensure 20MHz isn't forced on*/
  _PROTECTED_WRITE(CLKCTRL.OSC20MCTRLA, 0x00);

  // --- RTC/PIT enable ---
  while (RTC.STATUS > 0) {
    ; // Do nothing until RTC is available
  }

  RTC.CLKSEL = RTC_CLKSEL_INT32K_gc; // Select 32K clock

  RTC.PITCTRLA = RTC_PERIOD_CYC32768_gc | RTC_PITEN_bm;
  // Change clock prescaler & enable PIT
  
  _delay_ms(1); // Allow time for input_pullup to stabilize

  sei(); // Enable interrupts

  // --- Loop ---
  while (1) {
    if (btnPushed == 1) {

      // --- Button handling ---
      
      buttonPress(); // Wait until button released

      RTC.PITINTCTRL = RTC_PI_bm; // Enable periodic ISR
      btnPushed = 0;
      
      writeEEPROM(); // Log this run
      
      /*
      To read back EEPROM eventually:
      
      avrdude -C avrdude.conf -c jtag2updi -P /dev/ttyUSB0 -p t402 
      -U eeprom:r:Test.txt:h
      
      Note that it's both position 0 and 1 combined, 
      so ex. "0x38,0x4a" == 4a38 == 19000
      */

      // --- Main action ---
      pushPin |= PORT_ISC_BOTHEDGES_gc; // Enable pin ISR

      for (uint16_t blinkCounter=0;
      blinkCounter < (totalTime * 4); blinkCounter++) {
        if (btnPushed == 1) { // Interrupt this loop / cancel timer
          buttonPress();      // Wait until button released
          //beepBuzzer(buzzAmt);
          softReset();
        }
        // --- Pulse/blink LED ---
        if (blinkCounter > 1) pulseLED(); // Skip the first blink

        // One blink finished, sleep until next PIT
        goToSleep();
      } // Finished blinking for total time

      // --- Sound buzzer ---
      for (uint8_t buzzCount = 0; buzzCount < buzzCycle; buzzCount++) {
        beepBuzzer(buzzAmt);
        _delay_ms(shortDelay);
        goToSleep();
      }
      // --- Done with main timer ---
      softReset();
    }

    /// --- Finished all tasks ---

    pushPin |= PORT_ISC_BOTHEDGES_gc; // Enable pin ISR
    RTC.PITINTCTRL &= ~RTC_PI_bm; // Disable periodic ISR
    goToSleep();
  }
}
